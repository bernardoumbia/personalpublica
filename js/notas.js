// Importa la función getAllNotes desde tu módulo de Firebase
import { getAllNotes } from "./firebase.js";

// Función para renderizar las notas en el contenedor notasUsuario
const renderNotas = async () => {
  try {
    // Obtiene todas las notas
    const allNotes = await getAllNotes();

    // Selecciona el contenedor notasUsuario
    const notasUsuarioContainer = document.querySelector('.notasUsuario');

    // Limpia el contenido actual del contenedor

    // Itera sobre todas las notas y crea elementos para mostrarlas
    allNotes.forEach(note => {
      const notaElement = document.createElement('div');
      notaElement.classList.add('nota-card'); // Agrega una clase para estilizar

      // Usa template literals para estructurar el HTML de cada nota de manera más legible
      notaElement.innerHTML = `
        <div class="nota-info">
          <p><strong>Contenido:</strong> ${note.contenido}</p>
        </div>
      `;

      notasUsuarioContainer.appendChild(notaElement);
    });
  } catch (error) {
    console.error("Error al obtener y renderizar las notas: ", error);
  }
};

// Llama a la función para renderizar las notas cuando se cargue la página
document.addEventListener('DOMContentLoaded', renderNotas);
